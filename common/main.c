//Java Wrapper getdents.
//Adapted from : https://github.com/aidenbell/getdents/blob/master/src/getdents.c
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <gnu/libc-version.h>
#include <time.h>
#include <locale.h>
#include <unistd.h>

#define _GNU_SOURCE
#include <dirent.h>     /* Defines DT_* constants */
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/syscall.h>

//-----------------------------------------------------------------------------

struct linux_dirent {
   long           d_ino;
   off_t          d_off;
   unsigned short d_reclen;
   char           d_name[];
};
//-----------------------------------------------------------------------------
//Global vars
#define SYS_CALL_BUFFER_BYTE_SIZE  (1024*1024*5)
char * sys_call_buffer = NULL;

extern uint64_t storage_max_file_count;
extern uint16_t storage_max_file_size;
extern uint64_t current_storage_pos;
extern char * storage;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//External functions
//-----------------------------------------------------------------------------
static int my_compare(const void * a, const void *b){

	return strcmp((char*)a, (char*)b);
}

//-----------------------------------------------------------------------------
void sort(char* arr[], int n){

	qsort(arr,n,sizeof(const char*),my_compare);
}
//-----------------------------------------------------------------------------

void printStartInfo(void){

  time_t rawtime;
  struct tm * timeinfo;
  char buffer [80];
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );

  setlocale (LC_ALL,"C");
  printf ("\tFast file list wrapper version 0.1\n");
  printf ("\tGNU libc version: %s\n", gnu_get_libc_version());
  printf ("\tLocale is: %s\n", setlocale(LC_ALL,NULL) );
  strftime (buffer,80,"%c",timeinfo);
  printf ("\tDate is: %s\n",buffer);
  char cwd[1024];
  if (getcwd(cwd, sizeof(cwd)) != NULL) printf("\tCurrent working dir: %s\n", cwd);
  printf ("\tstorage_max_line_count: %lu\n", storage_max_file_count);
  printf ("\tstorage_max_line_size: %i\n", storage_max_file_size );

}

//-----------------------------------------------------------------------------

void fast_file_list_close(void) {

  if (sys_call_buffer != NULL) {
	  free(sys_call_buffer);
	  sys_call_buffer = NULL;
  }
}
//-----------------------------------------------------------------------------

int fast_file_list_init(uint32_t user_max_line_count, uint16_t user_max_line_size, int printInfo) {

  storage_max_file_count = user_max_line_count;
  storage_max_file_size = user_max_line_size;

  setlocale (LC_ALL,"C");
  if (printInfo) printStartInfo();

  sys_call_buffer = (char *) malloc(SYS_CALL_BUFFER_BYTE_SIZE);
  if (sys_call_buffer == NULL){
    printf ("\nError reserving memory for system call buffer. Step 1");
 	return -1;
   }
  return 1;
}

//-----------------------------------------------------------------------------
int fast_file_list_run(char * path, int sort_output){

  int fd, nread;
  struct linux_dirent *d;
  int bpos;
  char d_type;
  char * line;

  fd = open(path, O_RDONLY | O_DIRECTORY);
  if (fd == -1){
    printf ("\nError. The path is not a directory or error opening the path %s: ", path);
    return -1;
  }

  for ( ; ; ) {
    nread = syscall(SYS_getdents, fd, sys_call_buffer, SYS_CALL_BUFFER_BYTE_SIZE);
	if (nread == -1){
	  printf ("\nError. The path is not a directory or error opening the path %s: ", path);
      return -2;
	}
	if (nread == 0) break;

	for (bpos = 0; bpos < nread;) {
	  d = (struct linux_dirent *) (sys_call_buffer + bpos);
	  d_type = *(sys_call_buffer + bpos + d->d_reclen - 1);

	  if( d->d_ino != 0 && d_type == DT_REG ){
		  line = &(storage[storage_max_file_size * current_storage_pos]);
		  strcpy(line, d->d_name);
		  ++current_storage_pos;
	  }

	  if (current_storage_pos >= storage_max_file_count) {
		printf ("\nError. Not enough lines reserved. Current count: %lu: ", current_storage_pos);
	    return -4;
	  }
	  bpos += d->d_reclen;
    }
  }

  if (sort_output)
	  qsort(storage,current_storage_pos,storage_max_file_size,my_compare);

  return 1;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//end of file "main.c"
//-----------------------------------------------------------------------------
