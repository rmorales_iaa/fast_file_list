/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_converter_fastFileList_FastFileList__ */

#ifndef _Included_com_converter_fastFileList_FastFileList__
#define _Included_com_converter_fastFileList_FastFileList__
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_converter_fastFileList_FastFileList__
 * Method:    run
 * Signature: ([BISLjava/lang/String;ZZ)J
 */
JNIEXPORT jlong JNICALL Java_com_converter_fastFileList_FastFileList_00024_run
  (JNIEnv *, jobject, jbyteArray, jint, jshort, jstring, jboolean, jboolean);

#ifdef __cplusplus
}
#endif
#endif
