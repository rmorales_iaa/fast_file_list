//============================================================================
// <Description>     :<Main file for wcstools java wrapper>
// <File>            :<main.cpp>
// <Type>            :<c code>
// <Author>          :<Rafael Morales rmorales@iaa.es>
// <Creation date>   :<04 August 2017>
// <History>         :<None>
//============================================================================

//============================================================================
//Include section
//============================================================================
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
//============================================================================
//System include

//============================================================================
//User include
#include "com_common_util_FastFileList__.h"
//============================================================================
//System include

//============================================================================
//Declaration section
//============================================================================


//============================================================================
//External functions
//============================================================================

uint32_t storage_max_file_count;
uint16_t storage_max_file_size;
uint64_t current_storage_pos;
char * storage;

extern int fast_file_list_init(uint32_t user_max_file_count, uint16_t user_max_file_size, int printInfo);
extern int fast_file_list_run(char * path, int sort_output);
extern void fast_file_list_close(void);
//============================================================================
//Code section
//============================================================================
JNIEXPORT jlong JNICALL Java_com_common_util_FastFileList_00024_run
(JNIEnv * env
 , jobject obj

 , jbyteArray user_storage

 , jint       user_max_file_count
 , jshort     user_max_file_size

 , jstring    user_path

 , jboolean   user_sort_output
 , jboolean   user_printInfo) {

    char * local_path = (char *) (*env)->GetStringUTFChars( env, user_path , NULL ); //FITS file name
    char * local_storage = (char *) (*env)->GetByteArrayElements(env, user_storage, 0);
    if (local_storage == NULL) {
      printf("\n Error getting the storage");
      return -1;
    }

    //copy and init user info
    storage = local_storage;
    storage_max_file_count = user_max_file_count;
    storage_max_file_size = user_max_file_size;
    current_storage_pos = 0;
    uint64_t max_storage_size = storage_max_file_count * storage_max_file_size;
    memset(storage, 0, max_storage_size);

    int r = -1;

    if (fast_file_list_init(storage_max_file_count, storage_max_file_size, user_printInfo) > 0){
   	  r = fast_file_list_run(local_path, user_sort_output);
   	  fast_file_list_close();
    }

    (*env)->ReleaseStringUTFChars(env, user_path, local_path);
    (*env)->ReleaseByteArrayElements(env, user_storage, (jbyte *)local_storage, 0);

    if (r < 0)  return r;
    else return current_storage_pos;
}

//============================================================================
//============================================================================

//============================================================================
//End of file:main.c
//============================================================================
