//Java Wrapper for sextractor
//https://www.astromatic.net/software/sextractor
//-----------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <limits.h>
//-----------------------------------------------------------------------------
//Global vars

uint32_t storage_max_file_count=0;
uint16_t storage_max_file_size=0;
uint64_t current_storage_pos=0;
char * storage = NULL;

extern int fast_file_list_init(uint32_t user_max_line_count, uint16_t user_max_line_size, int printInfo);
extern int fast_file_list_run(char * path, int sort_output);
extern void fast_file_list_close(void);

//-----------------------------------------------------------------------------

void deallocate_memory(void) {

  if (storage != NULL){
	free(storage);
	storage = NULL;
  }
}
//-----------------------------------------------------------------------------

int allocate_memory(uint32_t user_max_line_count, uint16_t user_max_line_size) {

  uint64_t size = user_max_line_count * user_max_line_size;
  storage = (char *) malloc(size);
  if (storage == NULL){
    printf ("\nError reserving memory for storage");
	return -2;
  }
  return 1;
}

//-----------------------------------------------------------------------------
void test_1(char * path, int sort_output, int printInfo) {

  uint32_t i = 0;
  char * line;

  if (fast_file_list_init(storage_max_file_count, storage_max_file_size, printInfo) > 0){
	  fast_file_list_run(path, sort_output);

	  for(i=0;i<current_storage_pos;++i){

		  line = &(storage[storage_max_file_size * i]);
		  printf ("\nLine: %i -> %s", i, line);
	  }
	  fast_file_list_close();
  }
}
//-----------------------------------------------------------------------------
long time_diff(clock_t t1, clock_t t2){

	return ((double) t2-t1) / CLOCKS_PER_SEC * 1000;
}
//-----------------------------------------------------------------------------

int main(int argc, char *argv[]){

  struct timespec start_time, stop_time;

  //user input
  //char * user_path = "/home/rafa/Downloads/test_file/";
  char * user_path = "//mnt/data2TB/astrometry/test_moose/170000-435800+171814-460200_20160611/";
  uint32_t user_max_file_count= 4*1000*1000;
  uint16_t user_max_file_size = 40;
  int user_sort_output = 1;
  int user_printInfo = 1;
  if (allocate_memory(user_max_file_count,user_max_file_size) <0) return -1;

  //copy user info
  storage_max_file_count = user_max_file_count;
  storage_max_file_size = user_max_file_size;
  current_storage_pos = 0;
  uint64_t max_storage_size = user_max_file_count * user_max_file_size;
  memset(storage, 0, max_storage_size);

  //time measuring
  clock_gettime(CLOCK_REALTIME, &start_time);

  test_1(user_path, user_sort_output, user_printInfo);

  clock_gettime(CLOCK_REALTIME, &stop_time);
  if (stop_time.tv_nsec < start_time.tv_nsec) {
    stop_time.tv_nsec += 1000000000;
  	stop_time.tv_sec--;
  }

  printf ("\nTotal file list count: %lu", current_storage_pos);
  printf("\nEnd of processing. Elapsed time: %ld.%09ld s\n", (long)(stop_time.tv_sec - start_time.tv_sec),stop_time.tv_nsec - start_time.tv_nsec);

  deallocate_memory();

  return 1;
}

//-----------------------------------------------------------------------------
//end of file "main.c"
//-----------------------------------------------------------------------------
